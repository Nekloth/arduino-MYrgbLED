#include "MYrgbLED.h"


MYrgbLED notif(5,6,3);

void setup() {
  Serial.begin(9600);
}

void loop() {
      // GREEN Blink
      notif.setPulse(0,100,0,1000,0,1000);
      for (long i=0;i<40000;i++){ Serial.println(i);notif.process(); }

      // BLUE Pulse
      notif.setPulse(0,0,255,4000,1000,0);
      for (long  i=0;i<80000;i++){ Serial.println(i);notif.process(); }

      // RED demoniac Pulse
      notif.setPulse(255,0,0,500,200,500);
      for (long i=0;i<40000;i++){ Serial.println(i);notif.process(); }


}
