# Introduction

*This library is my first library development. *

The role of this library is to be able to *blink* or *pulse* a RGB led in an appropriate color and with time parameters (how long should it be off before lighting up, how long to light up and down ...). 

The main important (critical in fact for the project I intent to use it) is : the code should not be blocked during the blink/pulsing process ! **So, no `delay` usage** !


> Here is then the result of my developments. **ENJOY** !


# Usage

## Constructor

In your code, create a global variable to access your RGB led, with 3 parameters : 
1. pin number (PWM) for the RED
1. pin number (PWM) for the GREEN
1. pin number (PWM) for the BLUE


*For instance*: `MYrgbLED notification_led(5,6,3);`

(I made my test on an Arduino Leonardo; this explains the pin number I put in the example);


## Define the LED "animation"

### Method: setPulse

`.setPule(r,g,b,off_duration,switch_duration,on_duration)`

Define the PULSE parameters

* `r` : RED value, from `0` to `255
* `g` : GREEN value, from `0` to `255
* `b` : BLUE value, from `0` to `255
* `off_duration` : number of millisecond the LED will stay OFF berfore switching ON
* `swicth_duration` : number of millisecond for the LED to be from OFF to ON (and ON to OFF)
* `on_duration` : number of millisecond the LED will stay ON berfore switching OFF


### Method: switchOff

`.swicthOff()`

Just switch OFF the LED.

### Method: process

`.process()`

To be called permanently. This method will ensure the animation. It has to be placed in the `loop()` method. Otherwise, the magic will never happen.


## Example 1: basic BLINK

`notification_led.setPulse(0,100,0,1000,0,1000);`

Explanations:

* Green color (`0,100,0`)
* 1 second OFF (`1000`)
* From OFF to ON in 0 ms (`0`)
* 1 second ON (`1000`)


# Examples 

## Example 2: basic PULSE

`notification_led.setPulse(0,0,255,4000,1000,0);`

Explanations:

* Blue color (`0,0,255`)
* 4 second OFF (`4000`)
* From OFF to ON in 1 second (`1000`)
* Immediate to_off animation (`0`)
