/*
  myRGBled.h - Library for handling RGB
  Created by Nekloth, May 16, 2016.
*/

#ifndef MYrgbLED_h
#define MYrgbLED_h

#include "Arduino.h"

class MYrgbLED
{
	public:
		MYrgbLED(int r, int g, int b);
		void setColor(byte r, byte g, byte b);
    void setPulse(byte r, byte g, byte b, int on_duration, int duration, int off_duration);
    void switchOff();
    void process();
    
	private:
		int _pin_r;
		int _pin_g;
		int _pin_b;
   
		int _status_r;
		int _status_g;
		int _status_b;

    int  _a_duration;
    bool _a_direction;
    long _a_timer;
    bool _a_action_todo;
    byte _a_r;
    byte _a_g;
    byte _a_b;
    int _a_step;
    int _a_step_r;
    int _a_step_g;
    int _a_step_b;
    int _a_step_duration;
    int _a_on_duration;
    int _a_off_duration;
    int _a_status;
};

#endif
