#include "Arduino.h"
#include "MYrgbLED.h"

MYrgbLED::MYrgbLED(int r, int g, int b) {
	pinMode(r, OUTPUT);pinMode(g, OUTPUT);pinMode(b, OUTPUT);	
	_pin_r = r;_pin_g = g;_pin_b = b;
  analogWrite(_pin_r,0); analogWrite(_pin_g,0); analogWrite(_pin_b,0);
  _status_r = 0;_status_g = 0;_status_b = 0;
}

void MYrgbLED::setPulse(byte r, byte g, byte b, int on_duration, int duration, int off_duration) {
    _a_duration = duration;
    _a_direction = true;
    _a_timer = millis();
    _a_action_todo = true; 
    _a_r = r;_a_g = g;_a_b = b;
    _a_step = 0;
    _a_step_r = (int) r / 100;
    _a_step_g = (int) g / 100;
    _a_step_b = (int) b / 100;
    _a_step_duration = (int) duration / 100 ;
    _a_on_duration = on_duration;
    _a_off_duration = off_duration;
    _a_status = 1; // = in transition
}

void MYrgbLED::process(){

    if ( _a_status == 2 && millis() >= (_a_timer + _a_off_duration) ) {
      _a_status = 1;
    }

    if ( _a_status == 0 && millis() >= (_a_timer + _a_on_duration) ) {
      _a_status = 1;
    }
  
  
    if ( millis() >= (_a_timer + _a_step_duration) && _a_status==1) {
      _a_action_todo = true;
    }

    if ( _a_action_todo ) {

      Serial.println(_a_step);
      if ( _a_direction ) {
        _a_step++;
      } else {
        _a_step--;
      }

       setColor( (byte) _a_step_r * _a_step , (byte) _a_step_g * _a_step , (byte) _a_step_b * _a_step );
      if ( _a_step == 100 || _a_step == 0 ) _a_direction = !_a_direction;
      if ( _a_step == 100 ) _a_status = 2;
      if ( _a_step == 0 ) _a_status = 0;
      _a_timer = millis();
      _a_action_todo = false;
    }  
}



void MYrgbLED::setColor(byte r, byte g, byte b) {
  analogWrite(_pin_r,r);
  analogWrite(_pin_g,g);
  analogWrite(_pin_b,b);
  _status_r = r;
  _status_g = g;
  _status_b = b;
}

void MYrgbLED::switchOff() {
  setColor(0,0,0);
}
