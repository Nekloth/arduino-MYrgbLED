# Developers are welcome !

Coding is not my daily job. My code is for sure not optimized at all, I'm sure about.

If you prefer to contribute to my code, I'll more than happy to welcome you !

If you prefer to *clone* my code, no problem ! :)

# Contact
Feel free to contact me if needed ! For improvements, bugs and so one, please use the GitLab feature for that, it will be easier to manage.